﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Arc_Code_Challange.Calculations;

namespace Arc_Code_Challange.UserControls
{
    public partial class CalculateRectangle : UserControl
    {
        public CalculateRectangle()
        {
            InitializeComponent();
            labelArea.Text = "";
        }

        private void numericLength_ValueChanged(object sender, EventArgs e)
        {
            UpdateArea();
        }

        private void numericWidth_ValueChanged(object sender, EventArgs e)
        {
            UpdateArea();
        }

        private void UpdateArea()
        {
            CalcRectangle calcRectangle = new CalcRectangle();
            var area = calcRectangle.CalcArea(numericWidth.Value, numericLength.Value);
            labelArea.Text = area.ToString();
        }
    }
}
