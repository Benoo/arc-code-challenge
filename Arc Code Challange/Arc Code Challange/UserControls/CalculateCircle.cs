﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Arc_Code_Challange.Calculations;

namespace Arc_Code_Challange.UserControls
{
    public partial class CalculateCircle : UserControl
    {
        public CalculateCircle()
        {
            InitializeComponent();
            labelArea.Text = "";
        }

        private void numericCircleRadius_ValueChanged(object sender, EventArgs e)
        {
            UpdateArea();
        }

        private void UpdateArea()
        {
            CalcCircle calcCircle = new CalcCircle();
            var area = calcCircle.CalcArea(numericCircleRadius.Value);
            labelArea.Text = area.ToString();
        }
    }
}
