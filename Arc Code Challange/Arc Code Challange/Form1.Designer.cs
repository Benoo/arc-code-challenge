﻿
namespace Arc_Code_Challange
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelControlsContainer = new System.Windows.Forms.Panel();
            this.comboBoxCalcAreaList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // panelControlsContainer
            // 
            this.panelControlsContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControlsContainer.Location = new System.Drawing.Point(24, 99);
            this.panelControlsContainer.Name = "panelControlsContainer";
            this.panelControlsContainer.Size = new System.Drawing.Size(1254, 571);
            this.panelControlsContainer.TabIndex = 2;
            // 
            // comboBoxCalcAreaList
            // 
            this.comboBoxCalcAreaList.FormattingEnabled = true;
            this.comboBoxCalcAreaList.Location = new System.Drawing.Point(142, 34);
            this.comboBoxCalcAreaList.Name = "comboBoxCalcAreaList";
            this.comboBoxCalcAreaList.Size = new System.Drawing.Size(271, 33);
            this.comboBoxCalcAreaList.TabIndex = 3;
            this.comboBoxCalcAreaList.SelectedIndexChanged += new System.EventHandler(this.comboBoxCalcAreaList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select Shape";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1308, 696);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboBoxCalcAreaList);
            this.Controls.Add(this.panelControlsContainer);
            this.Name = "Form1";
            this.Text = "Calc Area";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel panelControlsContainer;
        private System.Windows.Forms.ComboBox comboBoxCalcAreaList;
        private System.Windows.Forms.Label label1;
    }
}

