﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Arc_Code_Challange.Function;
using Arc_Code_Challange.UserControls;

namespace Arc_Code_Challange
{
    public partial class Form1 : Form
    {
        CalculateCircle calculateCircle;
        CalculateRectangle calculateRectangle;

        public Form1()
        {
            InitializeComponent();
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            calculateCircle = new CalculateCircle();
            calculateRectangle = new CalculateRectangle();
            comboBoxCalcAreaList.Items.Add(new ComboBoxItem() { Text = "Circle", Value = calculateCircle });
            comboBoxCalcAreaList.Items.Add(new ComboBoxItem() { Text = "Rectangle", Value = calculateRectangle });
        }


        private void comboBoxCalcAreaList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBoxCalcAreaList.SelectedItem == null)
            {
                return;
            }
            panelControlsContainer.Controls.Clear();
            var calculateItem = (ComboBoxItem)comboBoxCalcAreaList.SelectedItem;
            Type calculateType = calculateItem.Value.GetType();
            
            if(calculateType == typeof(CalculateCircle))
            {
                var calculate = (CalculateCircle)calculateItem.Value;
                calculate.Dock = DockStyle.Fill;
                panelControlsContainer.Controls.Add(calculate);
            }
            if (calculateType == typeof(CalculateRectangle))
            {
                var calculate = (CalculateRectangle)calculateItem.Value;
                calculate.Dock = DockStyle.Fill;
                panelControlsContainer.Controls.Add(calculate);
            }
        }
    }
}
