﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arc_Code_Challange.Calculations
{
    public class CalcCircle : CalcBase
    {
        public decimal CalcArea(decimal radius)
        {
            return ((decimal)MathF.PI * radius * radius);
        }
    }
}
