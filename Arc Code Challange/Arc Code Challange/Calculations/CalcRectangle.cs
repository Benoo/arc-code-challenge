﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arc_Code_Challange.Calculations
{
    public class CalcRectangle : CalcBase
    {
        public decimal CalcArea(decimal width, decimal length)
        {
            return width * length;
        }
    }
}
